package arrayIndexList;

import indexList.IndexList;

public class ArrayIndexList<E> implements IndexList<E> {
	private static final int INITCAP = 1; 
	private static final int CAPTOAR = 1; 
	private static final int MAXEMPTYPOS = 2; 
	private E[] element; 
	private int size; 

	public ArrayIndexList() { 
		element = (E[]) new Object[INITCAP]; 
		size = 0; 
	} 
	

	public void add(int index, E e) throws IndexOutOfBoundsException {
		// ADD CODE AS REQUESTED BY EXERCISES
		if(index>this.element.length || index<0)
			throw new IndexOutOfBoundsException("add: invalid index = "+ index);
		
		if(size==element.length)
			changeCapacity(CAPTOAR);
		//Move Data to right and add element to position
		moveDataOnePositionTR(index, size-1);
		this.element[index] = e;
		size++;
		
	}


	public void add(E e) {
		// ADD CODE AS REQUESTED BY EXERCISES
		if(size==element.length)
			changeCapacity(CAPTOAR);
		element[size]=e;
		size++;
		
	}


	public E get(int index) throws IndexOutOfBoundsException {
		// ADD AND MODIGY CODE AS REQUESTED BY EXERCISES
		if(index < 0 || index >= this.size())
			throw new IndexOutOfBoundsException();
		return this.element[index]; 
	}


	public boolean isEmpty() {
		return size == 0;
	}


	public E remove(int index) throws IndexOutOfBoundsException {
		// ADD AND MODIFY CODE AS REQUESTED BY EXERCISES
		if((index < 0 || index >= this.size()) )
			throw new IndexOutOfBoundsException();
		if(element.length-size>=MAXEMPTYPOS){
			changeCapacity(-CAPTOAR);
		}
		E temp = this.element[index];
		moveDataOnePositionTL(index+1, size-1);
		this.element[size-1] = null;
		
		size--;
		return temp;
		
	}


	public E set(int index, E e) throws IndexOutOfBoundsException {
		if(index < 0 || index >= this.element.length)
			throw new IndexOutOfBoundsException();
		
		E temp = this.element[index];
		this.element[index] = e;
	
		return temp;
	}


	public int size() {
		return size;
	}	
	
	
	
	// private methods  -- YOU CAN NOT MODIFY ANY OF THE FOLLOWING
	// ... ANALYZE AND USE WHEN NEEDED
	
	// you should be able to decide when and how to use
	// following method.... BUT NEED TO USE THEM WHENEVER
	// NEEDED ---- THIS WILL BE TAKEN INTO CONSIDERATION WHEN GRADING
	
	private void changeCapacity(int change) { 
		int newCapacity = element.length + change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = element[i]; 
			element[i] = null; 
		} 
		element = newElement; 
	}
	
	// useful when adding a new element with the add
	// with two parameters....
	private void moveDataOnePositionTR(int low, int sup) { 
		// pre: 0 <= low <= sup < (element.length - 1)
		for (int pos = sup; pos >= low; pos--)
			element[pos+1] = element[pos]; 
	}

	// useful when removing an element from the list...
	private void moveDataOnePositionTL(int low, int sup) { 
		// pre: 0 < low <= sup <= (element.length - 1)
		for (int pos = low; pos <= sup; pos++)
			element[pos-1] = element[pos]; 
	}


		// The following two methods are to be implemented as part of an exercise
	public Object[] toArray() {
		// TODO es in Exercise 3
		Object[] Array =  new Object[this.capacity()];
		
		for(int i=0;i<this.size();i++){
			Array[i] = element[i];
		}
		
		for(int i =this.size(); i < this.capacity()  ; i++)
		{
			Array[i] = null; 
		}
		return Array;
	}


	@Override
	public <T> T[] toArray(T[] array) {
		// TODO as in Exercise 3
		if (array.length > this.size()){
			
			for(int i= 0; i < this.size(); i++)
			{
				array[i] = (T) this.element[i];
			}
			 return array;
		}
		// Make a new array of a's runtime type, but my contents:
		//E[] newArr = (E[]) new Object[this.size()];
		array = (T[]) Array.newInstance(array.getClass().getComponentType(), this.capacity()); 
		for(int i = 0; i < this.size() ; i++)
		{
			array[i] = (T) this.element[i]; 
		}
		
		for(int i =this.size(); i < this.capacity()  ; i++)
		{
			array[i] = null; 
		}
		
		
		return array;
	}

    public Object clone()
	{
		IndexList<E> clon = new ArrayIndexList<>();
		
		for(int i = 0; i < this.size(); i++)
		{
			clon.add(this.element[i]);
		}
		return clon;
	}
	
	public void clear()
	{
	    int tempSize = this.size();
		for(int i = 0 ; i <tempSize; i++)
		{
			this.element[i] = null;
			this.size--;
		}
	}
	
	public IndexList<E> sublist(int low, int upper)
	{
		IndexList<E> sub = new ArrayIndexList<>();
		for(int i =low; i < upper; i++)
		{
			sub.add(this.element[i]);
		}
		return sub;
	}


	@Override
	public int capacity() {
		
		return element.length; 
	}

}
